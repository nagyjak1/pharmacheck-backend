#!/bin/bash

REGISTRY_USERNAME="$1"
REGISTRY_PASSWORD="$2"
REGISTRY_URL="$3"

docker login -u "$REGISTRY_USERNAME" -p "$REGISTRY_PASSWORD" "$REGISTRY_URL"
docker-compose pull
docker-compose up -d

