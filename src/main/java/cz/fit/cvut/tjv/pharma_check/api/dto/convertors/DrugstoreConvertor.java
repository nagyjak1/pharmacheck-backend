package cz.fit.cvut.tjv.pharma_check.api.dto.convertors;

import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreWithoutDrugDto;
import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;


public class DrugstoreConvertor {

    public static DrugstoreDto toDrugstoreDto(Drugstore drugstore) {
        DrugstoreDto result = new DrugstoreDto();
        result.setId(drugstore.getId());
        result.setCity(drugstore.getCity());
        result.setDrugs(drugstore.getDrugs().stream().map(DrugConvertor::toDrugWithoutDrugstoreDto).toList());
        return result;
    }

    public static DrugstoreWithoutDrugDto toDrugstoreWithoutDrugDto(Drugstore drugstore) {
        DrugstoreWithoutDrugDto result = new DrugstoreWithoutDrugDto();
        result.setId(drugstore.getId());
        result.setCity(drugstore.getCity());
        return result;
    }

    public static Drugstore toDrugstore(DrugstoreSimpleDto dto, Drugstore old) {
        old.setCity(dto.getCity());
        return old;
    }
}
