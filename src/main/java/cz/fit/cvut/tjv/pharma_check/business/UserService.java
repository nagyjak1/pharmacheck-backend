package cz.fit.cvut.tjv.pharma_check.business;

import cz.fit.cvut.tjv.pharma_check.dao.PrescriptionJpaRepository;
import cz.fit.cvut.tjv.pharma_check.dao.UserJpaRepository;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;
import cz.fit.cvut.tjv.pharma_check.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class UserService extends AbstractCrudService<User, Long> {

    protected UserService(UserJpaRepository repository) {
        super(repository);
    }

}
