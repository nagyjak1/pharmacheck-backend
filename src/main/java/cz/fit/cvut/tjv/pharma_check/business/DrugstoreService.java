package cz.fit.cvut.tjv.pharma_check.business;

import cz.fit.cvut.tjv.pharma_check.dao.DrugstoreJpaRepository;
import cz.fit.cvut.tjv.pharma_check.domain.Drug;
import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class DrugstoreService extends AbstractCrudService<Drugstore, Long> {

    protected DrugService drugService;

    protected DrugstoreService(DrugstoreJpaRepository repository, DrugService drugService) {
        super(repository);
        this.drugService = drugService;
    }

    public void addDrug(Long drugstoreID, Long drugID) {
        Drugstore drugstore = readById(drugstoreID).orElseThrow();
        Drug drug = drugService.readById(drugID).orElseThrow();
        if ( drugstore.getDrugs().contains( drug ) )
            throw new EntityStateException("Drug already added");
        drugstore.getDrugs().add(drug);
        repository.save(drugstore);
    }

    public void removeDrug(Long drugstoreID, Long drugID) {
        Drugstore drugstore = readById(drugstoreID).orElseThrow();
        Drug drug = drugService.readById(drugID).orElseThrow();
        if ( !drugstore.getDrugs().contains(drug))
            throw new NoSuchElementException("Drug not found in the prescription");
        drugstore.getDrugs().remove(drug);
        repository.save(drugstore);
    }

}
