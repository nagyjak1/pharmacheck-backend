package cz.fit.cvut.tjv.pharma_check.api.controller;

import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.convertors.DrugstoreConvertor;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreWithoutDrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserDto;
import cz.fit.cvut.tjv.pharma_check.business.DrugstoreService;
import cz.fit.cvut.tjv.pharma_check.business.EntityStateException;
import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/drugstores")
public class DrugstoreController {

    DrugstoreService drugstoreService;

    public DrugstoreController(DrugstoreService drugstoreService) {
        this.drugstoreService = drugstoreService;
    }



    @Operation(summary = "Create new drugstore")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drugstore created",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DrugstoreDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Wrong request body",
                    content = @Content) })
    @PostMapping
    public DrugstoreDto create(@RequestBody DrugstoreSimpleDto dto) {
        try {
            Drugstore drugstore = drugstoreService.create(new Drugstore(dto.getCity()));
            return DrugstoreConvertor.toDrugstoreDto(drugstore);
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @Operation(summary = "Get all drugstores")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the drugstores",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = DrugstoreDto.class))) }) })
    @GetMapping
    public Collection<DrugstoreDto> readAll() {
        return drugstoreService.readAll().stream().map(DrugstoreConvertor::toDrugstoreDto).toList();
    }


    @Operation(summary = "Get drugstore by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the drugstore",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DrugstoreDto.class)) }),
            @ApiResponse(responseCode = "404", description = "Drugstore with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @GetMapping("/{id}")
    public DrugstoreDto readOne(@Parameter(description = "id of drugstore to be searched") @PathVariable Long id) {
        try {
            Drugstore drugstore = drugstoreService.readById(id).get();
            return DrugstoreConvertor.toDrugstoreDto(drugstore);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }



    @Operation(summary = "Edit drugstore")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drugstore edited",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Drugstore with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @PutMapping("/{id}")
    public void update(@RequestBody DrugstoreSimpleDto dto, @Parameter(description = "id of drugstore to be edited") @PathVariable Long id) {
        try {
            Drugstore drugstore = drugstoreService.readById(id).get();
            drugstoreService.update(DrugstoreConvertor.toDrugstore(dto, drugstore));
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }



    @Operation(summary = "Delete drugstore")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drugstore deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Drugstore with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @DeleteMapping("/{id}")
    public void delete(@Parameter(description = "id of drugstore to be deleted") @PathVariable Long id) {
        drugstoreService.deleteById(id);
    }



    @Operation(summary = "Add drug with given drug id to drugstore with given drugstore id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drug added to the drugstore",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Drug or drugstore with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @PutMapping("/{drugstore_id}/drugs/{drug_id}")
    public void addDrugToDrugstore(@Parameter(description = "id of drugstore") @PathVariable Long drugstore_id, @Parameter(description = "id of drug to be added") @PathVariable Long drug_id) {
        try {
            drugstoreService.addDrug(drugstore_id, drug_id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }



    @Operation(summary = "Delete drug with given drug id from drugstore with given drugstore id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drug deleted from drugstore",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Drug or drugstore with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @DeleteMapping("/{drugstore_id}/drugs/{drug_id}")
    public void deleteDrugFromDrugstore(@Parameter(description = "id of drugstore") @PathVariable Long drugstore_id, @Parameter(description = "id of drug to be deleted") @PathVariable Long drug_id) {
        try {
            drugstoreService.removeDrug(drugstore_id, drug_id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
