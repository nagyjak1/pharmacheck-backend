package cz.fit.cvut.tjv.pharma_check.api.dto.drug;

import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreWithoutDrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionWithoutDrugDto;

import java.util.Collection;
import java.util.Objects;

public class DrugDto {

    private Long id;

    private String name;

    private String description;

    private Collection<DrugstoreWithoutDrugDto> drugstores;

    private Collection<PrescriptionWithoutDrugDto> prescriptions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<DrugstoreWithoutDrugDto> getDrugstores() {
        return drugstores;
    }

    public void setDrugstores(Collection<DrugstoreWithoutDrugDto> drugstores) {
        this.drugstores = drugstores;
    }

    public Collection<PrescriptionWithoutDrugDto> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Collection<PrescriptionWithoutDrugDto> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public DrugDto(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public DrugDto() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrugDto drugDto = (DrugDto) o;
        return Objects.equals(id, drugDto.id) && Objects.equals(name, drugDto.name) && Objects.equals(description, drugDto.description) && Objects.equals(drugstores, drugDto.drugstores) && Objects.equals(prescriptions, drugDto.prescriptions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, drugstores, prescriptions);
    }
}
