package cz.fit.cvut.tjv.pharma_check.api.controller;

import cz.fit.cvut.tjv.pharma_check.api.dto.convertors.DrugConvertor;
import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserDto;
import cz.fit.cvut.tjv.pharma_check.business.DrugService;
import cz.fit.cvut.tjv.pharma_check.business.EntityStateException;
import cz.fit.cvut.tjv.pharma_check.domain.Drug;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.awt.print.Book;
import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/drugs")
public class DrugController {

    DrugService drugService;

    public DrugController(DrugService drugService) {
        this.drugService = drugService;
    }


    @Operation(summary = "Create new drug")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drug created",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DrugDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Wrong request body",
                    content = @Content) })
    @PostMapping
    public DrugDto create(@RequestBody DrugSimpleDto dto) {
        try {
            Drug drug = drugService.create(new Drug(dto.getName(), dto.getDescription()));
            return DrugConvertor.toDrugDto(drug);
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @Operation(summary = "Get all drugs")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the drugs",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = DrugDto.class))) }) })
    @GetMapping
    public Collection<DrugDto> readAll() {
        return drugService.readAll().stream().map(DrugConvertor::toDrugDto).toList();
    }

    @Operation(summary = "Get drug by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the drug",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DrugDto.class)) }),
            @ApiResponse(responseCode = "404", description = "Drug with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @GetMapping("/{id}")
    public DrugDto readOne(@Parameter(description = "id of book to be searched") @PathVariable Long id) {
        try {
            return DrugConvertor.toDrugDto(drugService.readById(id).get());
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Edit drug")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drug edited",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Drug with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @PutMapping("/{id}")
    public void update(@RequestBody DrugSimpleDto dto, @Parameter(description = "id of book to be edited") @PathVariable Long id) {
        try {
            Drug drug = drugService.readById(id).get();
            drugService.update(DrugConvertor.toDrug(dto, drug));
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Delete drug")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drug deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Drug with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @DeleteMapping("/{id}")
    public void delete(@Parameter(description = "id of book to be deleted") @PathVariable Long id) {
        drugService.deleteById(id);
    }

}

