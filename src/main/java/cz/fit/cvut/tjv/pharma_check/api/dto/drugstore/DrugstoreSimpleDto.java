package cz.fit.cvut.tjv.pharma_check.api.dto.drugstore;

import java.util.Objects;

public class DrugstoreSimpleDto {

    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrugstoreSimpleDto that = (DrugstoreSimpleDto) o;
        return Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city);
    }
}
