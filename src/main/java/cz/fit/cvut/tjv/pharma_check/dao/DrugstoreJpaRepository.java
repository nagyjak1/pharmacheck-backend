package cz.fit.cvut.tjv.pharma_check.dao;

import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DrugstoreJpaRepository extends JpaRepository<Drugstore, Long> {
}
