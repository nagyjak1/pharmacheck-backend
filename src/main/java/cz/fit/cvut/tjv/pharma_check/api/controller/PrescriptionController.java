package cz.fit.cvut.tjv.pharma_check.api.controller;

import cz.fit.cvut.tjv.pharma_check.api.dto.convertors.DrugstoreConvertor;
import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.convertors.PrescriptionConvertor;
import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserDto;
import cz.fit.cvut.tjv.pharma_check.business.EntityStateException;
import cz.fit.cvut.tjv.pharma_check.business.PrescriptionService;
import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/prescriptions")
public class PrescriptionController {

    PrescriptionService prescriptionService;

    public PrescriptionController(PrescriptionService prescriptionService) {
        this.prescriptionService = prescriptionService;
    }

    @Operation(summary = "Create new prescription")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Prescription created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PrescriptionDto.class))})})
    @PostMapping
    public PrescriptionDto create() {
        try {
            Prescription prescription = prescriptionService.create(new Prescription());
            return PrescriptionConvertor.toPrescriptionDto(prescription);
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }


    @Operation(summary = "Get all prescriptions")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the prescriptions",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = PrescriptionDto.class)))})})
    @GetMapping
    public Collection<PrescriptionDto> readAll() {
        return prescriptionService.readAll().stream().map(PrescriptionConvertor::toPrescriptionDto).toList();
    }


    @Operation(summary = "Get prescription by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the prescription",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PrescriptionDto.class))}),
            @ApiResponse(responseCode = "404", description = "Prescription with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @GetMapping("/{id}")
    public PrescriptionDto readOne(@Parameter(description = "id of prescription to be searched") @PathVariable Long id) {
        try {
            Prescription prescription = prescriptionService.readById(id).get();
            return PrescriptionConvertor.toPrescriptionDto(prescription);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    @Operation(summary = "Edit prescription")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Prescription edited",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Prescription with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @PutMapping("/{id}")
    public void update(@RequestBody PrescriptionSimpleDto dto, @Parameter(description = "id of prescription to be edited") @PathVariable Long id) {
        try {
            Prescription prescription = prescriptionService.readById(id).get();
            prescriptionService.update(PrescriptionConvertor.toPrescription(dto, prescription));
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Delete prescription")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Prescription deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Prescription with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @DeleteMapping("/{id}")
    public void delete(@Parameter(description = "id of prescription to be deleted") @PathVariable Long id) {
        prescriptionService.deleteById(id);
    }


    @Operation(summary = "Add drug with given drug id to prescription with given prescription id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drug added to the prescription",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Drug or prescription with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @PutMapping("/{p_id}/drugs/{d_id}")
    public void addDrugToPrescription(@Parameter(description = "id of prescription") @PathVariable Long p_id, @Parameter(description = "id of drug to be added") @PathVariable Long d_id) {
        try {
            prescriptionService.addDrug(p_id, d_id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }


    @Operation(summary = "Set prescription's user to user with given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User successfully set",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Prescription or user with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @PutMapping("/{p_id}/user/{u_id}")
    public void setUserToPrescription(@Parameter(description = "id of prescription") @PathVariable Long p_id, @PathVariable @Parameter(description = "id of user to be set") Long u_id) {
        try {
            prescriptionService.setUser(p_id, u_id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }


    @Operation(summary = "Unset prescription's user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User successfully unset",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Prescription with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @DeleteMapping("/{p_id}/user")
    public void removeUserFromPrescription(@Parameter(description = "id of prescription") @PathVariable Long p_id) {
        try {
            prescriptionService.removeUser(p_id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }




    @Operation(summary = "Delete drug with given drug id from prescription with given prescription id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drug deleted from prescription",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Drug or prescription with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @DeleteMapping("/{p_id}/drugs/{d_id}")
    public void deleteDrugFromPrescription(@Parameter(description = "id of prescription") @PathVariable Long p_id, @Parameter(description = "id of drug to be deleted") @PathVariable Long d_id) {
        try {
            prescriptionService.deleteDrug(p_id, d_id);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }




    @Operation(summary = "Get drugstores where all drugs from prescription with given id are available")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Drugstores found",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = DrugstoreDto.class)))}),
            @ApiResponse(responseCode = "404", description = "Prescription with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @GetMapping("/{p_id}/drugstores")
    public Collection<DrugstoreDto> getDrugstoresWithAllPrescribedDrugs(@Parameter(description = "id of prescription") @PathVariable Long p_id) {
        try {
            Collection<DrugstoreDto> result = prescriptionService.getDrugstoresFromPrescription(p_id).stream().map(DrugstoreConvertor::toDrugstoreDto).toList();
            return result;
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
