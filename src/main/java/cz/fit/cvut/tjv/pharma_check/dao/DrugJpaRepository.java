package cz.fit.cvut.tjv.pharma_check.dao;

import cz.fit.cvut.tjv.pharma_check.domain.Drug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DrugJpaRepository extends JpaRepository<Drug, Long> {
}
