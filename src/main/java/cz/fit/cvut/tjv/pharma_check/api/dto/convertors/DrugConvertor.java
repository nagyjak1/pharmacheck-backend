package cz.fit.cvut.tjv.pharma_check.api.dto.convertors;

import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugWithoutDrugstoreDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugWithoutPrescriptionDto;
import cz.fit.cvut.tjv.pharma_check.domain.Drug;

public class DrugConvertor {


    public static DrugDto toDrugDto(Drug drug) {
        DrugDto result = new DrugDto();

        result.setName(drug.getName());
        result.setId(drug.getId());
        result.setDescription(drug.getDescription());

        result.setPrescriptions(drug.getPrescriptions().stream().map(
                PrescriptionConvertor::toPrescriptionWithoutDrugDto).toList());

        result.setDrugstores(drug.getDrugstores().stream().map(
                DrugstoreConvertor::toDrugstoreWithoutDrugDto).toList());

        return result;
    }

    public static DrugWithoutPrescriptionDto toDrugWithoutPrescriptionDto(Drug drug) {

        DrugWithoutPrescriptionDto result = new DrugWithoutPrescriptionDto();

        result.setId(drug.getId());
        result.setDescription(drug.getDescription());
        result.setName(drug.getName());

        result.setDrugstores(drug.getDrugstores().stream().map(
                DrugstoreConvertor::toDrugstoreWithoutDrugDto).toList());

        return result;
    }

    public static DrugWithoutDrugstoreDto toDrugWithoutDrugstoreDto(Drug drug) {

        DrugWithoutDrugstoreDto result = new DrugWithoutDrugstoreDto();

        result.setId(drug.getId());
        result.setDescription(drug.getDescription());
        result.setName(drug.getName());

        result.setPrescriptions(drug.getPrescriptions().stream().map(
                PrescriptionConvertor::toPrescriptionWithoutDrugDto).toList());

        return result;
    }

    public static Drug toDrug(DrugSimpleDto drugDto, Drug drug) {
        drug.setName(drugDto.getName());
        drug.setDescription(drugDto.getDescription());
        return drug;
    }

}
