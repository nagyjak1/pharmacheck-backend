package cz.fit.cvut.tjv.pharma_check.api.controller;

import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.convertors.UserConvertor;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserWithoutPrescriptionDto;
import cz.fit.cvut.tjv.pharma_check.business.EntityStateException;
import cz.fit.cvut.tjv.pharma_check.business.UserService;
import cz.fit.cvut.tjv.pharma_check.domain.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/users")
public class UserController {

    UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Create new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User created",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Wrong request body",
                    content = @Content) })
    @PostMapping
    public UserDto create(@RequestBody UserSimpleDto dto) {
        try {
            User user = userService.create( new User ( dto.getUsername() ));
            return UserConvertor.toUserDto( user );

        } catch (EntityStateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }


    @Operation(summary = "Get all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the users",
                    content = { @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = UserDto.class))) }) })
    @GetMapping
    public Collection<UserDto> readAll() {
        return userService.readAll().stream().map(UserConvertor::toUserDto).toList();
    }


    @Operation(summary = "Get user by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the user",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class)) }),
            @ApiResponse(responseCode = "404", description = "User with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @GetMapping("/{id}")
    public UserDto readOne(@Parameter(description = "id of user to be searched") @PathVariable Long id) {
        try {
            User user = userService.readById(id).get();
            return UserConvertor.toUserDto( user );
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Edit user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User edited",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @PutMapping("/{id}")
    public void update(@RequestBody UserSimpleDto dto, @Parameter(description = "id of user to be edited") @PathVariable Long id) {
        try {
            User user = userService.readById( id ).get();
            userService.update( UserConvertor.toUser( dto, user ) );
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Delete user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User with given id not found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content)})
    @DeleteMapping("/{id}")
    public void delete(@Parameter(description = "id of user to be deleted") @PathVariable Long id) {
        userService.deleteById(id);
    }

}
