package cz.fit.cvut.tjv.pharma_check.business;

import cz.fit.cvut.tjv.pharma_check.dao.DrugJpaRepository;
import cz.fit.cvut.tjv.pharma_check.domain.Drug;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DrugService extends AbstractCrudService<Drug, Long> {


    protected DrugService( DrugJpaRepository repository) {
        super(repository);
    }

    @Override
    public Drug update(Drug entity) {
        return repository.save( entity );
    }
}
