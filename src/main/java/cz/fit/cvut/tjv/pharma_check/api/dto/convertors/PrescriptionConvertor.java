package cz.fit.cvut.tjv.pharma_check.api.dto.convertors;

import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionWithoutDrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionWithoutUserDto;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;

public class PrescriptionConvertor {

    public static PrescriptionDto toPrescriptionDto(Prescription prescription) {
        var dto = new PrescriptionDto();
        dto.setId(prescription.getId());
        dto.setEPrescriptionCode(prescription.getEPrescriptionUUID());
        dto.setDrugs(prescription.getDrugs().stream().map(DrugConvertor::toDrugWithoutPrescriptionDto).toList());
        if (prescription.getUser() != null)
            dto.setUser(UserConvertor.toUserWithoutPrescriptionDto(prescription.getUser()));
        else
            dto.setUser(null);
        return dto;
    }

    public static PrescriptionWithoutDrugDto toPrescriptionWithoutDrugDto(Prescription prescription) {
        var dto = new PrescriptionWithoutDrugDto();
        dto.setId(prescription.getId());
        dto.setEPrescriptionCode(prescription.getEPrescriptionUUID());
        dto.setUser(UserConvertor.toUserWithoutPrescriptionDto(prescription.getUser()));
        return dto;
    }

    public static PrescriptionWithoutUserDto toPrescriptionWithoutUserDto(Prescription prescription) {
        var dto = new PrescriptionWithoutUserDto();
        dto.setId(prescription.getId());
        dto.setEPrescriptionCode(prescription.getEPrescriptionUUID());
        dto.setDrugs(prescription.getDrugs().stream().map(DrugConvertor::toDrugWithoutPrescriptionDto).toList());
        return dto;
    }

    public static Prescription toPrescription(PrescriptionSimpleDto dto, Prescription old) {
        old.setEPrescriptionUUID( dto.getEPrescriptionCode() );
        return old;
    }

}
