package cz.fit.cvut.tjv.pharma_check.api.dto.prescription;

import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugWithoutPrescriptionDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserWithoutPrescriptionDto;

import java.util.Collection;
import java.util.Objects;
import java.util.UUID;

public class PrescriptionWithoutDrugDto {

    private Long id;

    private UUID EPrescriptionCode;

    private UserWithoutPrescriptionDto user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserWithoutPrescriptionDto getUser() {
        return user;
    }

    public void setUser(UserWithoutPrescriptionDto user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrescriptionWithoutDrugDto that = (PrescriptionWithoutDrugDto) o;
        return Objects.equals(id, that.id) && Objects.equals(EPrescriptionCode, that.EPrescriptionCode) && Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, EPrescriptionCode, user);
    }

    public UUID getEPrescriptionCode() {
        return EPrescriptionCode;
    }

    public void setEPrescriptionCode(UUID EPrescriptionCode) {
        this.EPrescriptionCode = EPrescriptionCode;
    }

}
