package cz.fit.cvut.tjv.pharma_check.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "user_tbl")
public class User implements DomainEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "user_seq")
    @SequenceGenerator(name = "user_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "username", unique = true)
    private String username;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Collection<Prescription> prescriptions = new ArrayList<>();

    public User() {
    }

    public User(Long id, String username, Collection<Prescription> prescriptions) {
        this.id = id;
        this.username = username;
        this.prescriptions = prescriptions;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Collection<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public User(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(username, user.username) && Objects.equals(prescriptions, user.prescriptions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, prescriptions);
    }
}