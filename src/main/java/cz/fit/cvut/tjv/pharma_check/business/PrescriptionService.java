package cz.fit.cvut.tjv.pharma_check.business;

import cz.fit.cvut.tjv.pharma_check.dao.PrescriptionJpaRepository;
import cz.fit.cvut.tjv.pharma_check.domain.Drug;
import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;
import cz.fit.cvut.tjv.pharma_check.domain.User;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class PrescriptionService extends AbstractCrudService<Prescription, Long> {

    protected DrugService drugService;
    protected DrugstoreService drugstoreService;
    protected UserService userService;

    protected PrescriptionService(PrescriptionJpaRepository repository, DrugService drugService, DrugstoreService drugstoreService, UserService userService) {
        super(repository);
        this.drugService = drugService;
        this.drugstoreService = drugstoreService;
        this.userService = userService;
    }

    @Override
    public Prescription create(Prescription entity) {
        entity.setEPrescriptionUUID( UUID.randomUUID() );
        return super.create(entity);
    }

    public void addDrug(Long prescriptionId, Long drugId) {
        Prescription prescription = readById(prescriptionId).orElseThrow();
        Drug drug = drugService.readById(drugId).orElseThrow();
        if (prescription.getDrugs().contains(drug))
            throw new EntityStateException("Drug already added");
        prescription.getDrugs().add(drug);
        repository.save(prescription);
    }

    public void setUser(Long prescriptionId, Long userId) {
        Prescription prescription = readById(prescriptionId).get();
        if (prescription.getUser() != null)
            throw new EntityStateException("User already set");
        User user = userService.readById(userId).get();
        prescription.setUser(user);
        repository.save(prescription);
    }

    public void removeUser(Long prescriptionId) {
        Prescription prescription = readById(prescriptionId).get();
        if (prescription.getUser() == null)
            throw new NoSuchElementException("User not found in the prescription");
        prescription.setUser(null);
        repository.save(prescription);
    }

    public void deleteDrug(Long prescriptionId, Long drugId) {
        Prescription prescription = readById(prescriptionId).orElseThrow();
        Drug drug = drugService.readById(drugId).orElseThrow();
        if (!prescription.getDrugs().contains(drug))
            throw new NoSuchElementException("Drug not found in the prescription");
        prescription.getDrugs().remove(drug);
        repository.save(prescription);
    }

    public Collection<Drugstore> getDrugstoresFromPrescription(Long prescriptionId) {
        if (!repository.existsById(prescriptionId))
            throw new NoSuchElementException("Prescription with given ID doesnt exist");
        return ((PrescriptionJpaRepository) repository).findDrugstoresWithAllPrescribedDrugs(prescriptionId);
    }

}
