package cz.fit.cvut.tjv.pharma_check.api.dto.convertors;

import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserWithoutPrescriptionDto;
import cz.fit.cvut.tjv.pharma_check.domain.User;

public class UserConvertor {

    public static UserDto toUserDto(User user) {
        if (user == null)
            return null;
        UserDto result = new UserDto();
        result.setId(user.getId());
        result.setUsername(user.getUsername());
        result.setPrescriptions(user.getPrescriptions().stream().map(PrescriptionConvertor::toPrescriptionWithoutUserDto).toList());
        return result;
    }

    public static UserWithoutPrescriptionDto toUserWithoutPrescriptionDto(User user) {
        if (user == null)
            return null;
        UserWithoutPrescriptionDto result = new UserWithoutPrescriptionDto();
        result.setId(user.getId());
        result.setUsername(user.getUsername());
        return result;
    }

    public static User toUser(UserSimpleDto dto, User user) {
        user.setUsername(dto.getUsername());
        return user;
    }

}
