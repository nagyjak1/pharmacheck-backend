package cz.fit.cvut.tjv.pharma_check.api.dto.drugstore;

import java.util.Objects;

public class DrugstoreWithoutDrugDto {

    private Long id;

    private String city;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrugstoreWithoutDrugDto that = (DrugstoreWithoutDrugDto) o;
        return Objects.equals(id, that.id) && Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, city);
    }
}
