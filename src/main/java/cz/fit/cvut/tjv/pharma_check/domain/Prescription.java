package cz.fit.cvut.tjv.pharma_check.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "prescription_tbl")
public class Prescription implements DomainEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "prescription_seq")
    @SequenceGenerator(name = "prescription_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    private UUID EPrescriptionUUID;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "prescription_tbl_drugs",
            joinColumns = @JoinColumn(name = "prescription_id"),
            inverseJoinColumns = @JoinColumn(name = "drugs_id"))
    private Collection<Drug> drugs = new ArrayList<>();

    public Prescription(Long id, User user, Collection<Drug> drugs) {
        this.id = id;
        this.user = user;
        this.drugs = drugs;
    }

    public Prescription() {
    }

    public Prescription(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<Drug> getDrugs() {
        return drugs;
    }

    public void setDrugs(Collection<Drug> drugs) {
        this.drugs = drugs;
    }

    public UUID getEPrescriptionUUID() {
        return EPrescriptionUUID;
    }

    public void setEPrescriptionUUID(UUID EPrescriptionUUID) {
        this.EPrescriptionUUID = EPrescriptionUUID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prescription that = (Prescription) o;
        return Objects.equals(id, that.id) && Objects.equals(EPrescriptionUUID, that.EPrescriptionUUID) && Objects.equals(user, that.user) && Objects.equals(drugs, that.drugs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, EPrescriptionUUID, user, drugs);
    }
}