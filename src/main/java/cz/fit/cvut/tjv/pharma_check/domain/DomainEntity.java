package cz.fit.cvut.tjv.pharma_check.domain;

import java.io.Serializable;

public interface DomainEntity <K> {
    public K getId();
}
