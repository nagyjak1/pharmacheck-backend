package cz.fit.cvut.tjv.pharma_check.dao;

import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface PrescriptionJpaRepository extends JpaRepository<Prescription, Long> {

    @Query("SELECT ds FROM Drugstore ds " +
            "JOIN ds.drugs d " +
            "JOIN d.prescriptions p " +
            "WHERE p.id = :prescription_id " +
            "GROUP BY ds.id, ds.city " +
            "HAVING COUNT(ds) = (SELECT COUNT(p) FROM Prescription p JOIN p.drugs WHERE p.id = :prescription_id)")
    List<Drugstore> findDrugstoresWithAllPrescribedDrugs(@Param("prescription_id") Long prescription_id);
}
