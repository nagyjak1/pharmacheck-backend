package cz.fit.cvut.tjv.pharma_check.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "drug_tbl")
public class Drug implements DomainEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drug_seq")
    @SequenceGenerator(name = "drug_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToMany(cascade=CascadeType.REMOVE, mappedBy = "drugs")
    private Collection<Drugstore> drugstores = new ArrayList<>();

    @ManyToMany(cascade=CascadeType.REMOVE, mappedBy = "drugs")
    private Collection<Prescription> prescriptions = new ArrayList<>();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Collection<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public Collection<Drugstore> getDrugstores() {
        return drugstores;
    }

    public void setDrugstores(Collection<Drugstore> drugstores) {
        this.drugstores = drugstores;
    }

    public Drug(Long id, String name, String description, Collection<Drugstore> drugstores, Collection<Prescription> prescriptions) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.drugstores = drugstores;
        this.prescriptions = prescriptions;
    }

    public Drug() {
    }

    public Drug(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Drug(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Drug drug = (Drug) o;
        return Objects.equals(id, drug.id) && Objects.equals(name, drug.name) && Objects.equals(description, drug.description) && Objects.equals(drugstores, drug.drugstores) && Objects.equals(prescriptions, drug.prescriptions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, drugstores, prescriptions);
    }
}