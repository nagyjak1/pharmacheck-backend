package cz.fit.cvut.tjv.pharma_check.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "drugstore_tbl")
public class Drugstore implements DomainEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drugstore_seq")
    @SequenceGenerator(name = "drugstore_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "drugstore_tbl_drugs",
            joinColumns = @JoinColumn(name = "drugstore_id"),
            inverseJoinColumns = @JoinColumn(name = "drugs_id"))
    private Collection<Drug> drugs = new ArrayList<>();

    @Column(name = "city")
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<Drug> getDrugs() {
        return drugs;
    }

    public void setDrugs(Collection<Drug> drugs) {
        this.drugs = drugs;
    }

    public Drugstore(Long id, Collection<Drug> drugs, String city) {
        this.id = id;
        this.drugs = drugs;
        this.city = city;
    }

    public Drugstore() {
    }

    public Drugstore(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Drugstore drugstore = (Drugstore) o;
        return Objects.equals(id, drugstore.id) && Objects.equals(drugs, drugstore.drugs) && Objects.equals(city, drugstore.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, drugs, city);
    }

    @Override
    public String toString() {
        return "Drugstore{" +
                "id=" + id +
                //", drugs=" + drugs +
                ", city='" + city + '\'' +
                '}';
    }
}