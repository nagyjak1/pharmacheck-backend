package cz.fit.cvut.tjv.pharma_check.api.dto.drug;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DrugDtoTest {

    @Test
    void testEquals() {
        DrugDto first = new DrugDto();
        DrugDto second = new DrugDto();
        first.setId(1L);
        first.setDescription("TestDescription");
        first.setName("TestName");
        second.setId(1L);
        second.setDescription("TestDescription");
        second.setName("TestName");
        assert( first.equals(second) );
    }

    @Test
    void testNotEquals() {
        DrugDto first = new DrugDto();
        DrugDto second = new DrugDto();
        first.setId(1L);
        first.setDescription("ErrorDescription");
        first.setName("TestName");
        second.setId(1L);
        second.setDescription("TestDescription");
        second.setName("TestName");
        assert( !first.equals(second) );
    }
}