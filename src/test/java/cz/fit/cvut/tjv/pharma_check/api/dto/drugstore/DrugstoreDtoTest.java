package cz.fit.cvut.tjv.pharma_check.api.dto.drugstore;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DrugstoreDtoTest {

    @Test
    void testEquals() {
        DrugstoreDto first = new DrugstoreDto();
        DrugstoreDto second = new DrugstoreDto();
        first.setId(1L);
        first.setCity("TestCity");
        second.setId(1L);
        second.setCity("TestCity");
        assert( first.equals(second));
    }

    @Test
    void testNotEqual() {
        DrugstoreDto first = new DrugstoreDto();
        DrugstoreDto second = new DrugstoreDto();
        first.setId(2L);
        first.setCity("TestCity");
        second.setId(1L);
        second.setCity("TestCity");
        assert( !first.equals(second));
    }

}