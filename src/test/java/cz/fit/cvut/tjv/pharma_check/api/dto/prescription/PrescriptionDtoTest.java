package cz.fit.cvut.tjv.pharma_check.api.dto.prescription;

import org.junit.jupiter.api.Test;

class PrescriptionDtoTest {

    @Test
    void testEquals() {
        PrescriptionDto first = new PrescriptionDto();
        PrescriptionDto second = new PrescriptionDto();
        first.setId(1L);
        second.setId(1L);
        assert (first.equals(second));
    }

    @Test
    void testNotEquals() {
        PrescriptionDto first = new PrescriptionDto();
        PrescriptionDto second = new PrescriptionDto();
        first.setId(0L);
        second.setId(1L);
        assert (!first.equals(second));
    }

}