package cz.fit.cvut.tjv.pharma_check.api.dto.convertors;

import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionWithoutDrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.prescription.PrescriptionWithoutUserDto;
import cz.fit.cvut.tjv.pharma_check.domain.Drug;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;
import cz.fit.cvut.tjv.pharma_check.domain.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrescriptionConvertorTest {

    @Test
    void toPrescriptionDto() {
        User user = new User(2L, "TestUser", null);
        Drug drug = new Drug( "TestDrug", "TestDescription");

        Prescription input = new Prescription();
        input.setId(1L);
        input.setUser(user);
        input.getDrugs().add(drug);

        PrescriptionDto result = PrescriptionConvertor.toPrescriptionDto( input );
        assertEquals( result.getId(), input.getId());
        assertEquals( result.getUser(), UserConvertor.toUserWithoutPrescriptionDto(input.getUser()));
        assertArrayEquals( result.getDrugs().toArray(), input.getDrugs().stream().map(DrugConvertor::toDrugWithoutPrescriptionDto).toArray());
    }

    @Test
    void toPrescriptionWithoutDrugDto() {
        User user = new User(2L, "TestUser", null);
        Drug drug = new Drug( "TestDrug", "TestDescription");

        Prescription input = new Prescription();
        input.setId(1L);
        input.setUser(user);
        input.getDrugs().add(drug);

        PrescriptionWithoutDrugDto result = PrescriptionConvertor.toPrescriptionWithoutDrugDto( input );
        assertEquals( result.getId(), input.getId());
        assertEquals( result.getUser(), UserConvertor.toUserWithoutPrescriptionDto(input.getUser()));
    }

    @Test
    void toPrescriptionWithoutUserDto() {
        User user = new User(2L, "TestUser", null);
        Drug drug = new Drug( "TestDrug", "TestDescription");

        Prescription input = new Prescription();
        input.setId(1L);
        input.setUser(user);
        input.getDrugs().add(drug);

        PrescriptionWithoutUserDto result = PrescriptionConvertor.toPrescriptionWithoutUserDto( input );
        assertEquals( result.getId(), input.getId());
        assertArrayEquals( result.getDrugs().toArray(), input.getDrugs().stream().map(DrugConvertor::toDrugWithoutPrescriptionDto).toArray());
    }
}