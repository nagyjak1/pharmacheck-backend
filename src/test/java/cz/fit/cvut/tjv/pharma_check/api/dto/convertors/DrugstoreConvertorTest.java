package cz.fit.cvut.tjv.pharma_check.api.dto.convertors;

import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drugstore.DrugstoreWithoutDrugDto;
import cz.fit.cvut.tjv.pharma_check.domain.Drug;
import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DrugstoreConvertorTest {

    @Test
    void toDrugstoreDto() {

        Drugstore input = new Drugstore();

        input.setCity("TestCity");
        input.setId(1L);

        Drug drug = new Drug( "TestDrug", "TestDescription");
        input.getDrugs().add( drug );

        DrugstoreDto result = DrugstoreConvertor.toDrugstoreDto( input );
        assertEquals( result.getCity(), input.getCity());
        assertEquals( result.getId(), input.getId() );
        assertArrayEquals( result.getDrugs().toArray(), input.getDrugs().stream().map( DrugConvertor::toDrugWithoutDrugstoreDto).toArray());
    }

    @Test
    void toDrugstoreWithoutDrugDto() {
        Drugstore input = new Drugstore();

        input.setCity("TestCity");
        input.setId(1L);

        DrugstoreWithoutDrugDto result = DrugstoreConvertor.toDrugstoreWithoutDrugDto( input );
        assertEquals( result.getCity(), input.getCity());
        assertEquals( result.getId(), input.getId() );
    }

    @Test
    void toDrugstore() {
        DrugstoreSimpleDto input = new DrugstoreSimpleDto();
        input.setCity( "TestCity" );

        Drugstore drugstore =  new Drugstore();
        drugstore.setId(1L);

        Drug drug = new Drug( "TestDrug", "TestDescription");
        drugstore.getDrugs().add( drug );

        Drugstore result = DrugstoreConvertor.toDrugstore( input, drugstore );
        assertEquals( result.getId(), drugstore.getId());
        assertEquals( result.getCity(), input.getCity() );
        assertArrayEquals( result.getDrugs().toArray(), drugstore.getDrugs().toArray());
    }
}