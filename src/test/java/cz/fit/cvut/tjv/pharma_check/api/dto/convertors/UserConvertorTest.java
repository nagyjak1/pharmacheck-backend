package cz.fit.cvut.tjv.pharma_check.api.dto.convertors;

import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.user.UserWithoutPrescriptionDto;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;
import cz.fit.cvut.tjv.pharma_check.domain.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserConvertorTest {

    @Test
    void toUserDto() {
        User input = new User();
        input.setId(1L);
        input.setUsername("TestUsername");
        Prescription prescription = new Prescription();
        input.getPrescriptions().add( prescription );

        UserDto result = UserConvertor.toUserDto( input );
        assertEquals( result.getId(), input.getId() );
        assertEquals( result.getUsername(), input.getUsername() );
        assertArrayEquals( result.getPrescriptions().toArray(), input.getPrescriptions().stream().map(PrescriptionConvertor::toPrescriptionWithoutUserDto).toArray());
    }

    @Test
    void toUserWithoutPrescriptionDto() {
        User input = new User();
        input.setId(1L);
        input.setUsername("TestUsername");
        Prescription prescription = new Prescription();
        input.getPrescriptions().add( prescription );

        UserWithoutPrescriptionDto result = UserConvertor.toUserWithoutPrescriptionDto( input );
        assertEquals( result.getId(), input.getId() );
        assertEquals( result.getUsername(), input.getUsername() );
    }

    @Test
    void toUser() {

        UserSimpleDto input = new UserSimpleDto();
        input.setUsername("TestUsername");

        User user = new User();
        user.setId(1L);

        User result = UserConvertor.toUser( input, user);
        assertEquals( result.getId(), user.getId());
        assertEquals( result.getUsername(), input.getUsername() );
    }
}