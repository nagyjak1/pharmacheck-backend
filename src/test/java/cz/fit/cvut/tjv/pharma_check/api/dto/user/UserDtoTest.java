package cz.fit.cvut.tjv.pharma_check.api.dto.user;

import cz.fit.cvut.tjv.pharma_check.domain.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDtoTest {

    @Test
    void testEquals() {
        UserDto first = new UserDto();
        UserDto second = new UserDto();
        first.setId(1L);
        first.setUsername("TEST");
        second.setId(1L);
        second.setUsername("TEST");
        assert(first.equals(second));
    }

    @Test
    void testNotEquals() {
        UserDto first = new UserDto();
        UserDto second = new UserDto();
        first.setId(1L);
        first.setUsername("test");
        second.setId(1L);
        second.setUsername("TEST");
        assert(!first.equals(second));
    }
}