package cz.fit.cvut.tjv.pharma_check.dao;

import cz.fit.cvut.tjv.pharma_check.ApiMain;
import cz.fit.cvut.tjv.pharma_check.domain.Drug;
import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;
import org.hibernate.Hibernate;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiMain.class)
public class PrescriptionJpaRepositoryIntegrationTest {

    @Autowired
    private PrescriptionJpaRepository prescriptionJpaRepository;

    @Autowired
    private DrugstoreJpaRepository drugstoreJpaRepository;

    @Autowired
    private DrugJpaRepository drugJpaRepository;

    @Test
    public void whenFindDrugstoresWithAllPrescribedDrugs_thenReturnDrugstores() {

        // given
        Drugstore drugstore = new Drugstore();
        drugstore.setCity("TestCity");
        drugstoreJpaRepository.save(drugstore);

        Drug drug = new Drug();
        drug.setName("TestName");
        drugJpaRepository.save(drug);

        Prescription prescription = new Prescription();
        prescription.setId(1L);
        prescriptionJpaRepository.save(prescription);

        prescription.getDrugs().add(drug);
        drugstore.getDrugs().add(drug);
        prescriptionJpaRepository.save(prescription);
        drugstoreJpaRepository.save(drugstore);

        //when
        Collection<Drugstore> found = prescriptionJpaRepository.findDrugstoresWithAllPrescribedDrugs(prescription.getId());

        //then
        Drugstore expected = drugstoreJpaRepository.findById(1L).get();
        Drugstore result = (Drugstore) found.toArray()[0];

        assertEquals( result.getId(), expected.getId() );
    }
}