package cz.fit.cvut.tjv.pharma_check.api.dto.convertors;

import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugSimpleDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugWithoutDrugstoreDto;
import cz.fit.cvut.tjv.pharma_check.api.dto.drug.DrugWithoutPrescriptionDto;
import cz.fit.cvut.tjv.pharma_check.domain.Drug;
import cz.fit.cvut.tjv.pharma_check.domain.Drugstore;
import cz.fit.cvut.tjv.pharma_check.domain.Prescription;
import cz.fit.cvut.tjv.pharma_check.domain.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DrugConvertorTest {

    @Test
    void toDrugDto() {
        Drug input = new Drug();
        input.setId(10L);
        input.setDescription("TestDescription");
        input.setName("TestName");

        User user = new User(2L, "TestUser", null);
        Prescription prescription = new Prescription(3L, user, null);
        Drugstore drugstore = new Drugstore(4L, null, "TestCity");

        input.getPrescriptions().add(prescription);
        input.getDrugstores().add(drugstore);

        DrugDto result = DrugConvertor.toDrugDto(input);
        assertEquals(result.getId(), input.getId());
        assertEquals(result.getName(), input.getName());
        assertEquals(result.getDescription(), input.getDescription());
        assertArrayEquals(result.getDrugstores().toArray(), input.getDrugstores().stream().map( DrugstoreConvertor::toDrugstoreWithoutDrugDto).toArray());
        assertArrayEquals(result.getPrescriptions().toArray(), input.getPrescriptions().stream().map( PrescriptionConvertor::toPrescriptionWithoutDrugDto).toArray());
    }

    @Test
    void toDrugWithoutPrescriptionDto() {
        Drug input = new Drug();
        input.setId(10L);
        input.setDescription("TestDescription");
        input.setName("TestName");

        User user = new User(2L, "TestUser", null);
        Prescription prescription = new Prescription(3L, user, null);
        Drugstore drugstore = new Drugstore(4L, null, "TestCity");

        input.getPrescriptions().add(prescription);
        input.getDrugstores().add(drugstore);

        DrugWithoutPrescriptionDto result = DrugConvertor.toDrugWithoutPrescriptionDto(input);
        assertEquals(result.getId(), input.getId());
        assertEquals(result.getName(), input.getName());
        assertEquals(result.getDescription(), input.getDescription());
        assertArrayEquals(result.getDrugstores().toArray(), input.getDrugstores().stream().map( DrugstoreConvertor::toDrugstoreWithoutDrugDto).toArray());
    }

    @Test
    void toDrugWithoutDrugstoreDto() {
        Drug input = new Drug();
        input.setId(10L);
        input.setDescription("TestDescription");
        input.setName("TestName");

        User user = new User(2L, "TestUser", null);
        Prescription prescription = new Prescription(3L, user, null);
        Drugstore drugstore = new Drugstore(4L, null, "TestCity");

        input.getPrescriptions().add(prescription);
        input.getDrugstores().add(drugstore);

        DrugWithoutDrugstoreDto result = DrugConvertor.toDrugWithoutDrugstoreDto(input);
        assertEquals(result.getId(), input.getId());
        assertEquals(result.getName(), input.getName());
        assertEquals(result.getDescription(), input.getDescription());
        assertArrayEquals(result.getPrescriptions().toArray(), input.getPrescriptions().stream().map( PrescriptionConvertor::toPrescriptionWithoutDrugDto).toArray());
    }

    @Test
    void toDrug() {
        DrugSimpleDto input = new DrugSimpleDto();
        input.setName("TestName");
        input.setDescription("TestDescription");

        Drug drug = new Drug();
        drug.setId(4L);

        Drug result = DrugConvertor.toDrug(input, drug);
        assertEquals(result.getId(), drug.getId());
        assertEquals(result.getName(), input.getName());
        assertEquals(result.getDescription(), input.getDescription());
    }
}